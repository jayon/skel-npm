const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

let config = {
  entry: {
    'login': './src/static/scripts/main.ts'
  },
  output: { 
    path: path.resolve(__dirname, '../static/scripts/')
  },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: {
          loader: 'ts-loader'
        }
      }
    ]
  }
};

if (process.env.ENV === 'production') {
  config.mode = 'production';
  config.optimization = {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false
          }
        }
      })
    ]
  };
}
else {
  config.mode = 'development';
}


module.exports = config;
